const express = require('express');
const bodyParser = require('body-parser');
// Configuring the database
const Config = require('./src/config.js');
const mongoose = require('mongoose');
const path = require("path");
var cors = require('cors')

// create express app
const app = express();
var whitelist = [Config.MobileUrl, Config.WebUrl, Config.APIUrl]

var corsOptions = {
  origin: function (origin, callback) {
    callback(null, true)
  }
}

app.use(cors(corsOptions));

app.use(function (req, res, next) {
  next();
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/uploads', express.static(__dirname + '/uploads'));
//Routing
require('./src/routes/user.route.js')(app);
require('./src/routes/upload.route.js')(app);

app.set('secretKey', 'nodeRestApi'); // jwt secret token

mongoose.Promise = global.Promise;


// Connecting to the database
mongoose.connect(Config.DBUrl, {
  useNewUrlParser: true
}).then(() => {
  console.log("Successfully connected to the database");
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});


// listen for requests
app.listen(Config.APIPort, () => {
  console.log("Server is listening on port " + Config.APIPort);
});
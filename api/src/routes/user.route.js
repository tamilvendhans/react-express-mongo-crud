module.exports = (app) => {

    const User = require('../controllers/user.controller.js');
    const JWTValidToken = require('../services/Validation').JWTValidToken;
    var bodyParser = require('body-parser')

    // create application/json parser
    var jsonParser = bodyParser.json()


    // create application/x-www-form-urlencoded parser
    var urlencodedParser = bodyParser.urlencoded({ extended: false })

    // User Data 

    app.get('/user/listuser',JWTValidToken, jsonParser, User.ListAllUser);
    app.get('/user/getUser',JWTValidToken, jsonParser, User.getUserById);
    app.post('/user/saveuser', jsonParser, User.SaveUser);

    app.put('/user/updateuser',JWTValidToken, jsonParser, User.UpdateUser);
    app.post('/user/deleteuser',JWTValidToken, jsonParser, User.DeleteUser);
    app.delete('/user/deleteAllUser',JWTValidToken, jsonParser, User.DeleteAllUsers);
    // User Login Provider Data 
    app.post('/user/loginuser', jsonParser, User.IsLogin);
}
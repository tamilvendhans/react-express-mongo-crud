module.exports = (app) => {
    const multipart = require('connect-multiparty');
    const Config = require('../config.js');
    var path = require('path')
    const multer = require('multer');
    var fs = require('fs')
    var DIR = path.resolve(__dirname, '../../uploads/');


    const storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, 'uploads');
        },
    
        // By default, multer removes file extensions so let's add them back
        filename: function(req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    });

    const removeFile = (filename) => {
        fs.rm(path(__dirname + "/uploads/" + filename), (res) => {
            console.log(res);
        })
    }

    app.post('/upload', (req, res) => {
        let upload = multer({ storage: storage }).single("profile-picture");
        if(req.body && req.body.oldProfileImagePath && req.body.removeOldImage == "true") {
            const filePathSplit = req.body.oldProfileImagePath.split("/")
            removeFile(filePathSplit[filePathSplit.length - 1]);
        }
        upload(req, res, function(err) {

            if (req.fileValidationError) {
                return res.send(req.fileValidationError);
            }
            else if (!req.file) {
                return res.send('Please select an image to upload');
            }
            else if (err instanceof multer.MulterError) {
                return res.send(err);
            }
            else if (err) {
                return res.send(err);
            }
    
            res.json({
                'path': Config.APIUrl + "/" + req.file.path.replace("\\","/")
            });
            // res.send(`You have uploaded this image: <hr/><img src="${req.file.path}" width="500"><hr /><a href="./">Upload another image</a>`);
        });
    });

    app.get('/downloadFile/:filename', (req, res) => {
        let filename = req.params.filename;
        res.download("./uploads/" + filename);
    })

    app.get('/GetAllfiles', (req, res) => {
        fs.readdir("./uploads", (err, files) => {
            for (let i = 0; i < files.length; ++i) {
                files[i] = Config.APIUrl + "/downloadFile/" + files[i];
            }

            res.send(files);
        })
    })

    app.post('/download', (req, res) => {

        var filePath = path.resolve(DIR, req.body.filename);
        //console.log(filePath)
        res.sendFile(filePath);
    })
}
let mongoose = require('mongoose');
let UserSchema = new mongoose.Schema({
    Name:String,
    Email: String,
    Phone: String,
    Password:String,
    image:String
}, { timestamps: true });
module.exports = mongoose.model('user', UserSchema);

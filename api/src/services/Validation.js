const Config = require('../config.js');
const Util = require('../services/Util.js');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');

function JWTValidToken(req, res, next) {

    // if (!Util.RequestBodyValid(req)) {
    //     return Util.SendStatus(res, 400, "error", "JWT  Token Request body is empty", req)
    // }

    // var token = "";

    // if (!Util.StringValid(req.headers['token'])) {
    //     return Util.SendStatus(res, 400, "error", "JWT Token Id is empty", req)
    // }

    // if (Util.StringValid(req.headers['token'])) {
    //     token = req.headers['token'];
    // }

    //  var token = req.headers['authorization'];
    //  console.log("ttt",token)
    // if (!token)
    //   return res.status(403).send({ auth: false, message: 'No token provided.' });
      
    // jwt.verify(token, Config.secret, function(err, decoded) {
    //   if (err)
    //   return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        
    //   // if everything good, save to request for use in other routes
    //   req.userId = decoded.id;
    //   next();
    // });

    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, Config.secret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
 
}

module.exports = {
    JWTValidToken: JWTValidToken
}


function IsNotNull(input) {
    if (input != null) {
        return true;
    }
    return false;
}

function IsNotUndefined(input) {
    if (IsNotNull(input) && input != undefined) {
        return true;
    }
    return false;
}

function IsNotEmpty(input) {
    if (IsNotUndefined(input) && input != "") {
        return true;
    }
    return false;
}

function IsLengthNotZero(input) {
    if (IsNotUndefined(input) && input.length != 0) {
        return true;
    }
    return false;
}

function SendStatus(res, code, status, message, data) {
    return res.status(code).send(
        {
            data: data,
            status: status,
            message: message
        }
    );
}

function RequestBodyValid(req) {
    if (IsNotUndefined(req) && IsNotUndefined(req.body)) {
        return true;
    }
    return false;
}

function SendError(res, err, name) {
    if (ObjectValid(err) && StringValid(err.kind) && err.kind === 'ObjectId') {
        return SendStatus(res, 404, "error", "Error Occured while processing (" + name + ")", err)
    }
    return SendStatus(res, 500, "error", "Fine Not Exist", err)
}

function SendRequestBodyError(res, req, name) {
    if (!RequestBodyValid(req)) {
        return SendStatus(res, 400, "error", "Request body is empty (" + name + ")", req.body)
    }
}

function ObjectValid(obj) {
    if (IsNotNull(obj) && IsNotUndefined(obj)) {
        return true;
    }
    return false;
}

function ArrayValid(obj) {
    if (StringValid(obj) && obj.length != 0) {
        return true;
    }
    return false;
}

function StringValid(obj) {
    if (ObjectValid(obj) && obj != "") {
        return true;
    }
    return false;
}

function ConvertToEmpty(obj) {
    if (!StringValid(obj)) {
        return "";
    }
    return obj;
}

module.exports = {
    IsNotNull: IsNotNull,
    IsNotUndefined: IsNotUndefined,
    IsNotEmpty: IsNotEmpty,
    IsLengthNotZero: IsLengthNotZero,
    SendStatus: SendStatus,
    RequestBodyValid: RequestBodyValid,
    SendError: SendError,
    SendRequestBodyError: SendRequestBodyError,
    ObjectValid: ObjectValid,
    ArrayValid: ArrayValid,
    StringValid: StringValid
}
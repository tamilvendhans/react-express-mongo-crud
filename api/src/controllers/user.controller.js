const User = require('../models/user.model.js');

let mongoose = require('mongoose');
const Config = require('../config.js');
const Util = require('../services/Util.js');
var jwt = require('jsonwebtoken');
exports.ListAllUser = (req, res) => {
    Util.SendRequestBodyError(res, req, "#ListAllUser#");
    User.find({}).then(User => {
        if (!Util.ObjectValid(User)) {
            return Util.SendStatus(res, 200, "failed", "User Not Fount",User)
        }
        
        return Util.SendStatus(res, 200, "success", "All User", User)
    }).catch(err => { return Util.SendError(res, err, "#ListAllUser#") });
    
};

exports.getUserById = (req, res) => {
    Util.SendRequestBodyError(res, req, "#ListAllUser#");
    User.findById(req.query.id).then(User => {
        if (!Util.ObjectValid(User)) {
            return Util.SendStatus(res, 200, "failed", "User Not Fount",User)
        }
        
        return Util.SendStatus(res, 200, "success", "User", User)
    }).catch(err => { return Util.SendError(res, err, "#ListAllUser#") });
    
};

exports.SaveUser = (req, res) => {
    Util.SendRequestBodyError(res, req, "#SaveUser#");
    const objUser = new User(req.body);
    objUser.save()
    .then(data => {
        return Util.SendStatus(res, 200, "success", "User Saved Successfully !!", data)
    }).catch(err => { return Util.SendError(res, err, "#SaveUser#") });
    // User.findOne({
    //     Email: req.body.Email
    // }).then(User => {
    //     if (!Util.ObjectValid(User)) {
    //        // return Util.SendStatus(res, 200, "SSS", "Emsdxist", User)
    //         objUser.save()
    //         .then(data => {
    //             return Util.SendStatus(res, 200, "success", "User Saved Successfully !!", data)
    //         }).catch(err => { return Util.SendError(res, err, "#SaveUser#") });
    //     }
    //     return Util.SendStatus(res, 200, "failed", "Email Already exist", User)
    // }).catch(err => { return Util.SendError(res, err, "#UserExistCheck#") });
    
};


exports.UpdateUser = (req, res) => {
    Util.SendRequestBodyError(res, req, "#UpdateUser#");
    console.log(req.body);

    if (!Util.StringValid(req.body._id)) {
        return Util.SendStatus(res, 400, "error", "UserGuid is not provided", req.body)
    }
    const objUser = new User(req.body);
   
    const objUpdateUser = {
        Name: objUser.Name,
        image: objUser.image,
        Email: objUser.Email,
        Phone: objUser.Phone,
        Password:objUser.Password
    };
    User.updateOne({ _id: req.body._id }, objUpdateUser, { new: true })
        .then(data => {
           
            return Util.SendStatus(res, 200, "success", "User Profile Saved Successfully !!", data)
        }).catch(err => { return Util.SendError(res, err, "#UpdateUser#") });
};

exports.DeleteUser = (req, res) => {
    Util.SendRequestBodyError(res, req, "#DeleteUser#");
    console.log(req.body);
    if (!Util.StringValid(req.body._id)) {
        return Util.SendStatus(res, 400, "error", "UserGuid is not provided", req.body)
    }
  
    User.deleteOne({ _id: req.body._id })
        .then(data => {
           
            return Util.SendStatus(res, 200, "success", "User Deleted!!")
        }).catch(err => { return Util.SendError(res, err, "#DeleteUser#") });
};

exports.DeleteAllUsers = (req, res) => {
    Util.SendRequestBodyError(res, req, "#DeleteUser#");
  
    User.deleteMany()
        .then(data => {
           
            return Util.SendStatus(res, 200, "success", "All Users are Deleted!!")
        }).catch(err => { return Util.SendError(res, err, "#DeleteUser#") });
};

// Users Login ProviderTo DB
exports.IsLogin = (req, res) => {
    Util.SendRequestBodyError(res, req, "#UserExistCheck#");
    if (!Util.StringValid(req.body.Email) || !Util.StringValid(req.body.Password)) {
        return Util.SendStatus(res, 200, "error", "Email/Password is not provided", req.body)
    }

    User.findOne({
        Email: req.body.Email,
        Password:req.body.Password
    }).then(User => {
        
        if (!Util.ObjectValid(User)) {
            return Util.SendStatus(res, 200, "success", "User  Not Exist", User)
        }
        console.log("RR-",User)
        var token = jwt.sign({ id: User._id }, Config.secret, {
            expiresIn: 86400 // expires in 24 hours
          });
     
        return Util.SendStatus(res, 200, "success", "User Exist", Object.assign(User._doc,{token: token}))
    }).catch(err => { return Util.SendError(res, err, "#UserExistCheck#") });
};

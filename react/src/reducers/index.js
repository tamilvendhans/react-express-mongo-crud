import { combineReducers } from "redux";
import users from "./crud";

export default combineReducers({
  users,
});

import React, { Component } from "react";
import { connect } from "react-redux";
import { retrieveUsers } from "../actions/users";
import makeAPICall, { asyncLocalStorage } from "../http-common";

class Login extends Component {
  constructor(props) {
    super(props);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.login = this.login.bind(this);

    this.state = {
      id: null,
      email: "",
      password: ""
    };
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  login(e) {
    e.preventDefault();
    const { email, password } = this.state;

    makeAPICall.post("user/loginuser",{ Email: email, Password: password})
        .then(async (data) => {
            console.log(data.data, data.data.data);
            const token = data.data && data.data.data && data.data.data.token || '';
            if(token) {
                asyncLocalStorage.setItem("user_auth_token", token).then(() => {
                    this.props.retrieveUsers();
                    this.props.history.push("/");
                });
            } else {
                alert(data.data.message)
            }
        }).catch((e) => {
            console.log(e);
        });
  }

  render() {
    return (
      <div className="submit-form">
          <form onSubmit={this.login}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                className="form-control"
                id="email"
                required
                value={this.state.email}
                onChange={this.onChangeEmail}
                name="email"
              />
            </div>

            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                className="form-control"
                id="password"
                required
                value={this.state.password}
                onChange={this.onChangePassword}
                name="password"
              />
            </div>

            <button type="submit" className="btn btn-success">
              Login
            </button>
          </form>
      </div>
    );
  }
}

export default connect(null, { retrieveUsers })(Login);

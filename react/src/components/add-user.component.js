import axios from "axios";
import React, { Component } from "react";
import { connect } from "react-redux";
import { createUser } from "../actions/users";
import makeAPICall from "../http-common";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onImageChange = this.onImageChange.bind(this);
    this.saveUser = this.saveUser.bind(this);
    this.reset = this.reset.bind(this);

    this.state = {
      id: null,
      Name: "",
      Email: "",
      Phone: "",
      Password: "",
      confirmPassword: "",
      image: "",
      submitted: false,
    };
  }

  onChangeInput(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onImageChange(e) {
    this.setState({
      image: e.target.files[0]
    });
  }

  saveUser(e) {
    e.preventDefault();
    const { Name, Email, Phone, Password, confirmPassword, image } = this.state;
    if(Password == confirmPassword) {
      if(image) {
        var formData = new FormData();
        formData.append("profile-picture", image);
        axios({
          method: "post",
          url: "http://localhost:3200/upload", 
          data: formData,
          headers: {
            "Content-Type": "multipart/form-data"
          }
        }).then((imageData) => {
          console.log(imageData);
          this.props
            .createUser({ Name, Email, Phone, Password, image: (imageData.data && imageData.data.path || "")  })
            .then((data) => {
              this.setState({
                id: data.id,
                Name: data.Name,
                Email: data.Email,
                Phone: data.Phone,
                Password: data.Password,
                image: data.image,
                submitted: true,
              });
              console.log(data);
            })
            .catch((e) => {
              console.log(e);
            });
        })
        .catch((e) => {
          console.log(e);
        }); 
      } else {
        this.props
          .createUser({ Name, Email, Phone, Password, image  })
          .then((data) => {
            this.setState({
              id: data.id,
              Name: data.Name,
              Email: data.Email,
              Phone: data.Phone,
              Password: data.Password,
              image: data.image,
              submitted: true,
            });
            console.log(data);
          })
          .catch((e) => {
            console.log(e);
          });
      }
    } else {
      alert("Password not match")
    }
  }

  reset() {
    this.setState({
      id: null,
      Name: "",
      Email: "",
      Phone: "",
      Password: "",
      confirmPassword: "",
      image: "",
      submitted: false,
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Your form has been submitted successfully!</h4>
            <button className="btn btn-success mt-2" onClick={this.reset}>
              Add Another User
            </button>
          </div>
        ) : (
          <form onSubmit={this.saveUser}>
            <div className="form-group">
              <label htmlFor="Name">Name</label>
              <input
                type="text"
                className="form-control"
                id="Name"
                required
                defaultValue={this.state.Name}
                onChange={this.onChangeInput}
                name="Name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="Email">Email</label>
              <input
                type="email"
                className="form-control"
                id="Email"
                required
                defaultValue={this.state.Email}
                onChange={this.onChangeInput}
                name="Email"
              />
            </div>

            <div className="form-group">
              <label htmlFor="Phone">Phone</label>
              <input
                type="number"
                className="form-control"
                id="Phone"
                defaultValue={this.state.Phone}
                onChange={this.onChangeInput}
                name="Phone"
                maxLength="10"
              />
            </div>

            <div className="form-group">
              <label htmlFor="Password">Password</label>
              <input
                type="password"
                className="form-control"
                id="Password"
                required
                defaultValue={this.state.Password}
                onChange={this.onChangeInput}
                name="Password"
              />
            </div>

            <div className="form-group">
              <label htmlFor="confirmPassword">Confirm Password</label>
              <input
                type="password"
                className="form-control"
                id="confirmPassword"
                defaultValue={this.state.confirmPassword}
                onChange={this.onChangeInput}
                name="confirmPassword"
              />
            </div>

            <div className="form-group">
              <label htmlFor="image">Image</label>
              <input
                type="file"
                className="form-control border-0"
                id="image"
                onChange={this.onImageChange}
                name="image"
                accept="image/*"
              />
            </div>

            <button type="submit" className="btn btn-success">
              Add User
            </button>
          </form>
        )}
      </div>
    );
  }
}

export default connect(null, { createUser })(AddUser);

import axios from "axios";
import React, { Component } from "react";
import { connect } from "react-redux";
import { updateUser, deleteUser } from "../actions/users";
import UserDataService from "../services/user.service";

class Crud extends Component {
  constructor(props) {
    super(props);
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onImageChange = this.onImageChange.bind(this);
    this.getUser = this.getUser.bind(this);
    this.updateContent = this.updateContent.bind(this);
    this.removeUser = this.removeUser.bind(this);

    this.state = {
      currentUser: {
        id: null,
        Name: "",
        Email: "",
        Phone: "",
        Password: "",
        image: "",
      },
      message: "",
      confirmPassword: "",
      previousImage: "",
      isImageChanged: false
    };
  }

  componentDidMount() {
    this.getUser(this.props.match.params.id);
  }

  onChangeInput(e) {
    if(e.target.name != "confirmPassword") {
      this.setState(function (prevState) {
        return {
          currentUser: {
            ...prevState.currentUser,
            [e.target.name]: e.target.value,
          },
        };
      });
    } else{
      this.setState({ [e.target.name]: e.target.value });
    }
  }

  onImageChange(e) {
    this.setState(function (prevState) {
      return {
        currentUser: {
          ...prevState.currentUser,
          image: e.target.files[0]
        },
      };
    });
    this.setState({
      isImageChanged: true
    });
  }

  getUser(id) {
    UserDataService.get(id)
      .then((response) => {
        this.setState({
          currentUser: response.data.data,
        });
        this.setState({
          previousImage: this.state.currentUser.image
        })
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateContent(e) {
    e.preventDefault();
    const { previousImage, isImageChanged, currentUser, confirmPassword } = this.state;
    console.log(currentUser.Password, "Cnf" + confirmPassword)
    if(currentUser.Password == confirmPassword) {
      if(isImageChanged) {
        var formData = new FormData();
        formData.append("profile-picture", currentUser.image);
        formData.append("oldProfileImagePath", previousImage);
        formData.append("removeOldImage", "true");
        axios({
          method: "post",
          url: "http://localhost:3200/upload", 
          data: formData,
          headers: {
            "Content-Type": "multipart/form-data"
          }
        }).then((imageData) => {
          console.log(imageData);
          currentUser.image = imageData.data && imageData.data.path || ""
          this.props
            .updateUser(currentUser)
            .then((data) => {
              alert("The user was updated successfully!");
              console.log(data);
            })
            .catch((e) => {
              console.log(e);
            });
        })
        .catch((e) => {
          console.log(e);
        });
      } else {
        this.props
          .updateUser(currentUser)
          .then((response) => {
            console.log(response);
            
            alert("The user was updated successfully!");
          })
          .catch((e) => {
            console.log(e);
          });
      }
    } else {
      alert("Password not match")
    }
  }

  removeUser() {
    this.props
      .deleteUser(this.state.currentUser._id)
      .then(() => {
        this.props.history.push("/");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentUser } = this.state;

    return (
      <div>
        {currentUser ? (
          <div className="edit-form">
            <h4>User</h4>
            <form onSubmit={this.updateContent}>
              <div className="form-group">
                <label htmlFor="Name">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="Name"
                  required
                  defaultValue={currentUser.Name}
                  onChange={this.onChangeInput}
                  name="Name"
                />
              </div>

              <div className="form-group">
                <label htmlFor="Email">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="Email"
                  required
                  defaultValue={currentUser.Email}
                  onChange={this.onChangeInput}
                  name="Email"
                />
              </div>

              <div className="form-group">
                <label htmlFor="Phone">Phone</label>
                <input
                  type="number"
                  className="form-control"
                  id="Phone"
                  defaultValue={currentUser.Phone}
                  onChange={this.onChangeInput}
                  name="Phone"
                  maxLength="10"
                />
              </div>

              <div className="form-group">
                <label htmlFor="Password">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="Password"
                  required
                  defaultValue=""
                  onChange={this.onChangeInput}
                  name="Password"
                />
              </div>

              <div className="form-group">
                <label htmlFor="confirmPassword">Confirm Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="confirmPassword"
                  defaultValue={this.state.confirmPassword}
                  onChange={this.onChangeInput}
                  name="confirmPassword"
                />
              </div>

              <div className="form-group">
                <label htmlFor="image">Image</label>
                <input
                  type="file"
                  className="form-control border-0"
                  id="image"
                  onChange={this.onImageChange}
                  name="image"
                  accept="image/*"
                />
              </div>
              <span className="btn btn-danger btn-sm mr-2" onClick={this.removeUser}>
                Delete
              </span>

              <button type="submit" className="btn btn-success btn-sm">
                Update
              </button>
            </form>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p className="m-3 text-muted text-center">Please click on a User...</p>
          </div>
        )}
      </div>
    );
  }
}

export default connect(null, { updateUser, deleteUser })(Crud);

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  retrieveUsers,
  findUsersByTitle,
  deleteAllUsers,
} from "../actions/users";
import { Link } from "react-router-dom";

class UserList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.setActiveUser = this.setActiveUser.bind(this);
    this.findByTitle = this.findByTitle.bind(this);
    this.removeAllUsers = this.removeAllUsers.bind(this);

    this.state = {
      currentUser: null,
      currentIndex: -1,
      searchTitle: "",
    };
  }

  componentDidMount() {
    this.props.retrieveUsers();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  refreshData() {
    this.setState({
      currentUser: null,
      currentIndex: -1,
    });
  }

  setActiveUser(user, index) {
    this.setState({
      currentUser: user,
      currentIndex: index,
    });
  }

  removeAllUsers() {
    this.props
      .deleteAllUsers()
      .then((response) => {
        console.log(response);
        this.refreshData();
        localStorage.removeItem("user_auth_token");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  findByTitle() {
    this.refreshData();

    this.props.findUsersByTitle(this.state.searchTitle);
  }

  render() {
    const { searchTitle, currentUser, currentIndex } = this.state;
    const { users } = this.props;

    return (
      <div className="row my-4">
        {/*<div className="col-md-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search by title"
              value={searchTitle}
              onChange={this.onChangeSearchTitle}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.findByTitle}
              >
                Search
              </button>
            </div>
          </div>
        </div>*/}
        <div className="col-md-6">
          <h4 className="my-3">Users List <Link to="/addUser" className="btn btn-sm rounded-lg btn-info float-right">+ Add new user</Link></h4>
          {users && users.length > 0 ? <React.Fragment><ul className="list-group">
            {users &&
              users.map((user, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveUser(user, index)}
                  key={index}
                >
                {user.image ? <img src={user.image} className="img-thumbnail img profile-image"/> : null}
                  <div className="d-inline-flex pl-3">
                    <h5>{user.Name}</h5>
                  </div>
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllUsers}
          >
            Remove All
          </button>
          </React.Fragment> : <div className="text-muted m-4">Please login to show the users list</div>}
        </div>
        <div className="col-md-6">
          {currentUser ? (
            <div>
              <h5>Selected User</h5>
              <table className="table table-bordered">
                <tr>
                  <td><label className="text-muted">Name: </label></td>
                  <td>{currentUser.Name}</td>
                </tr>
                <tr>
                  <td><label className="text-muted">Mail: </label></td>
                  <td>{currentUser.Email}</td>
                </tr>
                <tr>
                  <td><label className="text-muted">Mobile: </label></td>
                  <td>{currentUser.Phone}</td>
                </tr>
              </table>

              <Link
                to={"/users/" + currentUser._id}
                className="btn btn-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              {users && users.length > 0 ? <p className="text-muted m-3">Please click on a User...</p> : null}
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
  };
};

export default connect(mapStateToProps, {
  retrieveUsers,
  findUsersByTitle,
  deleteAllUsers,
})(UserList);

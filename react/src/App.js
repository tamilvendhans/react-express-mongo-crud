import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AddUser from "./components/add-user.component";
import Crud from "./components/crud.component";
import UserList from "./components/user-list.component";
import userService from './services/user.service'
import loginComponent from "./components/login.component";
import { emptyUsers } from "./actions/users";
import { connect } from "react-redux";

class App extends Component {

  constructor() {
    super();
    this.state = {
      isUserExist: userService.isUserLoggedIn()
    }

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    this.props.emptyUsers();
    localStorage.removeItem("user_auth_token");
    this.setState({isUserExist: false});
  }

  render() {
    const { isUserExist } = this.state
    const { users } = this.props;
    console.log(isUserExist)
    return (
      <Router>
        <nav className="navbar navbar-dark bg-dark">
          <h2 className="navbar-brand">
            App Innovation Technologies
          </h2>
          <ul className="nav justify-content-end">
            <li className="nav-item">
              <Link to={"/users"} className="nav-link text-white">
                Users List
              </Link>
            </li>
            <li className="nav-item">
              { !isUserExist && !users.length ? <Link to={"/addUser"} className="nav-link text-white">
                Register
              </Link> : null }
            </li>
            <li className="nav-item">
              { !isUserExist && !users.length ? <Link to={"/login"} className="nav-link text-white">
                Login
              </Link> : <span className="nav-link text-white" onClick={this.handleLogout}>Logout</span> }
            </li>
          </ul>
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/users"]} component={UserList} />
            <Route path="/login" component={loginComponent} />
            <Route path="/addUser" component={AddUser} />
            <Route path="/users/:id" component={Crud} />
          </Switch>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
  };
};

export default connect(mapStateToProps, {emptyUsers})(App);

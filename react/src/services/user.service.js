import http, {makeAuthorizedAPICall} from "../http-common";

class UserDataService {
  getAll() {
    return makeAuthorizedAPICall.get("/user/listuser");
  }

  get(id) {
    return makeAuthorizedAPICall.get(`/user/getUser?id=${id}`);
  }

  create(data) {
    return http.post("/user/saveuser", data);
  }

  update(data) {
    return makeAuthorizedAPICall.put(`/user/updateuser`, data);
  }

  delete(data) {
    return makeAuthorizedAPICall.post(`/user/deleteuser`, data);
  }

  deleteAll() {
    return makeAuthorizedAPICall.delete(`/user/deleteAllUser`);
  }

  findByTitle(title) {
    return makeAuthorizedAPICall.get(`/users?title=${title}`);
  }

  isUserLoggedIn() {
    return localStorage.getItem("user_auth_token") != null ? true : false;
  }

}

export default new UserDataService();

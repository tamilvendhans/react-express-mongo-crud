import axios from "axios";

const makeAPICall = axios.create({
  baseURL: "http://localhost:3200/",
  headers: {
    "Content-type": "application/json"
  }
});

const makeAuthorizedAPICall = axios.create({
  baseURL: "http://localhost:3200/",
  headers: {
    "Content-type": "application/json",
    "Authorization": `Bearer ${(localStorage.getItem("user_auth_token") || "")}`
  }
});

const asyncLocalStorage = {
  setItem: function (key, value) {
      return Promise.resolve().then(function () {
          localStorage.setItem(key, value);
      });
  },
  getItem: function (key) {
      return Promise.resolve().then(function () {
          return localStorage.getItem(key);
      });
  }
};

export default makeAPICall
export {
  makeAPICall,
  makeAuthorizedAPICall,
  asyncLocalStorage
}
import {
  CREATE_USER,
  RETRIEVE_USER,
  UPDATE_USER,
  DELETE_USER,
  DELETE_ALL_USERS
} from "./types";

import UserDataService from "../services/user.service";
export const createUser = (data) => async (dispatch) => {
  try {
    const res = await UserDataService.create(data);

    retrieveUsers()
    // dispatch({
    //   type: CREATE_USER,
    //   payload: res.data,
    // });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const retrieveUsers = () => async (dispatch) => {
  try {
    const res = await UserDataService.getAll();
    dispatch({
      type: RETRIEVE_USER,
      payload: res.data.data,
    });
  } catch (err) {
    console.log(err);
  }
};

export const emptyUsers = () => async (dispatch) => {
  try {
    dispatch({
      type: RETRIEVE_USER,
      payload: [],
    });
  } catch (err) {
    console.log(err);
  }
};

export const updateUser = (data) => async (dispatch) => {
  try {
    const res = await UserDataService.update(data);

    dispatch({
      type: UPDATE_USER,
      payload: data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteUser = (id) => async (dispatch) => {
  try {
    await UserDataService.delete({ _id: id });

    dispatch({
      type: DELETE_USER,
      payload: { id },
    });
  } catch (err) {
    console.log(err);
  }
};

export const deleteAllUsers = () => async (dispatch) => {
  try {
    const res = await UserDataService.deleteAll();

    dispatch({
      type: DELETE_ALL_USERS,
      payload: res.data,
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const findUsersByTitle = (title) => async (dispatch) => {
  try {
    const res = await UserDataService.findByTitle(title);

    dispatch({
      type: RETRIEVE_USER,
      payload: res.data,
    });
  } catch (err) {
    console.log(err);
  }
};